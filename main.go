package main

import (
	"os"

	"gitlab.com/hatricker/etherbeat/cmd"

	_ "gitlab.com/hatricker/etherbeat/include"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
