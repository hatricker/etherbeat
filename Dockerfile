FROM golang:1.13-alpine AS builder

COPY . /go/src/gitlab.com/hatricker/etherbeat

RUN apk update && apk upgrade && \
    apk add --no-cache ca-certificates bash git openssh build-base && \
    cd /go/src/gitlab.com/hatricker/etherbeat && \
    make

FROM alpine:latest
RUN apk --no-cache add ca-certificates bash
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/hatricker/etherbeat/etherbeat* ./

ENTRYPOINT [ "/root/etherbeat" ]
CMD [ "-c", "/root/etherbeat.yml", "-e"]